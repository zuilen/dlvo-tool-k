﻿using DLVO.Model;
using DLVO.models;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DLVO
{
    public class LineCalculator
    {
        DlvoModel model;

        public LineCalculator( DlvoModel model)
        {
            this.model = model;
        }

        public List<LineSeries> CalculateGraphData()
        {

            List<LineSeries> graphs = new List<LineSeries>();
            //if (model.MultipleGraphs && model.VariableParameters.Count > 0)//enables DLVOModolizer to add multiple graphs based on the same formula
            //    for (int i = 0; i < model.VariableParameters.Count; i++)
            //    {

            //        model.GetType().GetProperty(model.VariableParameters[i].PropertyName).SetValue(model ,model.VariableParameters[i].Value);
                    
            //        graphs.AddRange(CalcSingleGraph(graphs));
            //    }
            //else
                graphs = CalcSingleGraph(graphs);
            return graphs;
        }



        private double Derivative(IFormula f, double dh, double h)//function to approach spline at specific point
        {
            double dh2 = dh * 2;
            return (f.Calculate(h - dh2) - 8 * f.Calculate(h - dh) + 8 * f.Calculate(h + dh) - f.Calculate(h + dh2)) / (dh2 * 6);
        }
        private LineSeries CalcLine(IFormula formula, bool Energy)
        {
            ChartValues<MeasureModel> values = new ChartValues<MeasureModel>();
            double steps = model.HMax / model.Resolution;
            for (double H = 0; H <= model.HMax; H += steps)
            {
                //If Energy needs to be calculated to normal formula is used,
                //:
                //when force needs to be calculated, method Derivitive will be called
                double y = Energy ? formula.Calculate(AbsoluteDistance(H)) : Derivative(formula, AbsoluteDistance(steps) * 1E-5, AbsoluteDistance(H));
                if (double.IsInfinity(y) == true || y > 100 || y <-100) //livechart does not compile infinite doubles
                    y = double.NaN; //turn inf value into NotaNumber, livechart discards this value instead of not compiling
                var m = new MeasureModel { H = H, Y = y};
                values.Add(m);
            }
            string energyorforce = Energy ? "Energy" : "Force";
            return new LineSeries { Values = values, Title = formula.NameInLegend() + " " + energyorforce };
        }
        private double RelativeDistance(double h)
        {
            return h / model.ParticleRadius;
        }
        private double AbsoluteDistance(double H)
        {
            return H * model.ParticleRadius;
        }

        private List<LineSeries> CalcSingleGraph(List<LineSeries> graphs)
        {
            if (model.VanderWaals)
                graphs.Add(CalcVanderWaals());
            if (model.DoubleLayer)
                graphs.Add(CalcDoubleLayer());
            if (model.BornRepulsion)
                graphs.Add(CalcBornRepulsion());
            if (model.RepulsiveHydration)
                graphs.Add(CalcRepulsiveHydration());
            if (model.DLVOFunction)
                graphs.Add(CalcDLVOFunction());
            if (model.ExtDLVOFunction)
                graphs.Add(CalcExtDLVOFunction());
            return graphs;
        }

        private LineSeries CalcExtDLVOFunction()
        {
            IFormula extdlvo = new ExtDLVOFormula
            (
                new VanDerWaals
                (
                    model.Lambda,
                    model.Hamaker,
                    model.ParticleRadius
                ),
                new DoubleLayer
                (
                    model.ParticleRadius,
                    model.ElementaryChargeConstant,
                    model.DielectricConstantFluid,
                    model.Z,
                    model.SurfacePotentialParticle,
                    model.SurfacePotentialWall,
                    model.BoltzmannConstant,
                    model.Temperature,
                    model.AvogadroConstant,
                    model.IonicStrength,
                    model.Vacuumpermittivity
                ),
                new BornRepulsion
                (
                    model.ParticleRadius,
                    model.BornCollisionRange,
                    model.Hamaker
                ),
                new RepulsiveHydration
                (
                    model.ParticleRadius,
                    model.ParticleForceConstant,
                    model.WallForceConstant,
                    model.ParticleDecayLength,
                    model.WallDecayLength
                )
            );
            return CalcLine(extdlvo, model.Energy);
        }

        private LineSeries CalcDLVOFunction()
        {
            IFormula dlvo = new DLVOFormula
            (
                new VanDerWaals
                (
                    model.Lambda,
                    model.Hamaker,
                    model.ParticleRadius
                ),
                new DoubleLayer
                (
                    model.ParticleRadius,
                    model.ElementaryChargeConstant,
                    model.DielectricConstantFluid,
                    model.Z,
                    model.SurfacePotentialParticle,
                    model.SurfacePotentialWall,
                    model.BoltzmannConstant,
                    model.Temperature,
                    model.AvogadroConstant,
                    model.IonicStrength,
                    model.Vacuumpermittivity
                )
            );
            return CalcLine(dlvo, model.Energy);
        }

        private LineSeries CalcRepulsiveHydration()
        {
            IFormula rh = new RepulsiveHydration
            (
                model.ParticleRadius,
                model.ParticleForceConstant,
                model.WallForceConstant,
                model.ParticleDecayLength,
                model.WallDecayLength
            );
            return CalcLine(rh, model.Energy);
        }

        private LineSeries CalcBornRepulsion()
        {
            IFormula br = new BornRepulsion
            (
                model.ParticleRadius,
                model.BornCollisionRange,
                model.Hamaker
            );
            return CalcLine(br, model.Energy);
        }

        private LineSeries CalcDoubleLayer()
        {
            IFormula dl = new DoubleLayer
            (
                model.ParticleRadius,
                model.ElementaryChargeConstant,
                model.DielectricConstantFluid,
                model.Z,
                model.SurfacePotentialParticle,
                model.SurfacePotentialWall,
                model.BoltzmannConstant,
                model.Temperature,
                model.AvogadroConstant,
                model.IonicStrength,
                model.Vacuumpermittivity
            );
            return CalcLine(dl, model.Energy);
        }

        private LineSeries CalcVanderWaals()
        {
            IFormula vdw = new VanDerWaals
            (
                model.Lambda,
                model.Hamaker,
                model.ParticleRadius
            );
            return CalcLine(vdw, model.Energy);
        }
    }
}

﻿using DLVO.Command;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DLVO.ViewModel
{
    /// <summary>
    /// codecasing: 
    ///     Private fields: lowercase
    ///     Locale Variables: lowercase
    ///     Parameters: camelCase
    ///     Public properties: PascalCase
    ///     Namespaces: PascalCase
    ///     Classes: PascalCase
    ///     Methods: PascalCase
    /// </summary>
    public class DlvoViewModel : INotifyPropertyChanged
    {
        //Properties

        //models
        public DlvoModel DlvoModel
        {
            get { return model; }
            set
            {
                if(value != null)
                {
                    model = value;
                    NotifyPropertyChanged("DlvoModel");
                    Plot();
                }
            }
        }

        public ChartViewModel ChartViewModel
        {
            get { return chartviewmodel; }
            set { chartviewmodel = value; }
        }
        //Commands
        public ICommand OpenCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand PlotCommand { get; set; }
        public ICommand ClearPlotCommand { get; set; }
        //public ICommand AddCommand { get; set; }

        public DlvoViewModel()
        {
            //modelviews
            ChartViewModel = new ChartViewModel();

            //models
            DlvoModel = new DlvoModel();

            //commands
            OpenCommand = new OpenCommand(this);
            SaveCommand = new SaveCommand(this);
            PlotCommand = new PlotCommand(this);
            ClearPlotCommand = new ClearPlotCommand(this);
            //AddCommand = new AddCommand(this);

        }
        private DlvoModel model;
        private ChartViewModel chartviewmodel;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(String info)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(info));
        }
        public void Open()
        {
            this.DlvoModel = OpenandSave.Openfile();
        }
        public void Save()
        {
            OpenandSave.Savefile(this.DlvoModel);
        }
        public void Plot()
        {
            var calculator = new LineCalculator(this.DlvoModel);
            List<LineSeries> graphs = calculator.CalculateGraphData();
            foreach (LineSeries s in graphs)
                ChartViewModel.Table.Add(s);
            ChartViewModel.ZoomMode = ZoomingOptions.Y;

        }
        public void ClearPlot()
        {
            ChartViewModel.Table.Clear();            
        }
        //public void AddToParameterList()
        //{
        //    //example TESTESTETESTTESTESTTESst
        //    this.DlvoModel.VariableParameters.Add(new VariableParameterModel(this.DlvoModel.Temperature.ToString(), 298));
        //    //end example
        //}

    }


}
